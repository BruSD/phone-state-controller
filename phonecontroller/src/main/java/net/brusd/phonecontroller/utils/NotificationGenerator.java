package net.brusd.phonecontroller.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.MainActivity;
import net.brusd.phonecontroller.R;

public class NotificationGenerator {


    private static Context context;

    public static void createNotification(Context _context, int type) {
        context = _context;
        String title = context.getResources().getString(R.string.notification_title);;
        String msg = "";
        switch (type){
            case Constant.MODE_FULL:

                msg = context.getResources().getString(R.string.full_volume_mode_string);
                break;

            case  Constant.MODE_MEDIUM:

                msg = context.getResources().getString(R.string.medium_volume_mode_string);
                break;

            case Constant.MODE_SILENT:

                msg = context.getResources().getString(R.string.silent_volume_mode_string);
                break;

        }
        notifi(title, msg);


    }

    private static void notifi(String whatHepenning, String msg) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

// build notification
// the addAction re-use the same intent to keep the example short
        Notification n = new Notification.Builder(context)
                .setContentTitle(whatHepenning)
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true).build();


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }
}
