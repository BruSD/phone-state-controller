package net.brusd.phonecontroller.utils;

import android.content.*;
import android.media.AudioManager;
import android.preference.PreferenceManager;

import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.R;


public class VolumeLevelController {

    public static void changVolumeLevel(Context context, int modeID) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        int volumeLvlA = 0;
        int volumeLvlR = 0;
        int volumeLvlN = 0;

        switch (modeID) {
            case Constant.MODE_FULL:
                volumeLvlA = am.getStreamMaxVolume(AudioManager.STREAM_ALARM);
                volumeLvlR = am.getStreamMaxVolume(AudioManager.STREAM_RING);
                volumeLvlN = am.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
                break;
            case Constant.MODE_MEDIUM:
                volumeLvlA = am.getStreamMaxVolume(AudioManager.STREAM_ALARM)/2;
                volumeLvlR = am.getStreamMaxVolume(AudioManager.STREAM_RING)/2;
                volumeLvlN = am.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION)/2;
                break;
            case Constant.MODE_SILENT:
                volumeLvlA = am.getStreamMaxVolume(AudioManager.STREAM_ALARM)*0;
                volumeLvlR = am.getStreamMaxVolume(AudioManager.STREAM_RING)*0;
                volumeLvlN = am.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION)*0;
                break;

        }
        int showUI = 0;

        android.content.SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if(prefs.getBoolean(context.getString(R.string.change_volume_level_ui), true)){
            showUI = AudioManager.FLAG_SHOW_UI;
        }

        am.setStreamVolume(AudioManager.STREAM_ALARM,volumeLvlA, showUI );
        am.setStreamVolume(AudioManager.STREAM_RING, volumeLvlR, showUI );
        am.setStreamVolume(AudioManager.STREAM_NOTIFICATION, volumeLvlN, showUI);
    }
}
