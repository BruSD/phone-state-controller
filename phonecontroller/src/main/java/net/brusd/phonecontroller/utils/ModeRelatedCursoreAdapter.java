package net.brusd.phonecontroller.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.AppDataBase.WiFiContract;
import net.brusd.phonecontroller.R;


public class ModeRelatedCursoreAdapter extends CursorAdapter {

    private Activity context;
    private boolean useTodayLayout;

    public ModeRelatedCursoreAdapter(Activity context, Cursor c, int flags) {

        super(context, c, flags);
        this.context = context;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {


        int layoutId = -1;
        // TODO: Determine layoutId from viewType

        layoutId = R.layout.item_releted_wifi_list;
        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        final String wifiName = cursor.getString(cursor.getColumnIndex(WiFiContract.WiFiEntry.COLUME_WIFI_NAME));

        viewHolder.dateView.setText(wifiName);
        viewHolder.iconView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = cursor.getInt(cursor.getColumnIndex(WiFiContract.WiFiEntry.COLUME_MODE_ID));
                QueryHelper.deleteWiFi(wifiName, context);
                refreshList(id);
            }
        });


    }

    public void refreshList(int modeID){

        Cursor cursor = QueryHelper.getCursoreWithWifiReletedToMode(modeID, context);
        swapCursor(cursor);
    }
    public static class ViewHolder {
        public final ImageView iconView;
        public final TextView dateView;


        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.delete_btn);
            dateView = (TextView) view.findViewById(R.id.wifi_name_text_veiw);

        }
    }
}
