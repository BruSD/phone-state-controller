package net.brusd.phonecontroller.AppDataBase;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class WiFiProvider extends ContentProvider {


    // // Константы для БД
    // БД
    static final String DB_NAME = "PhoneStateController";
    static final int DB_VERSION = 1;
    //// UriMatcher
    // общий Uri
    static final int URI_WIFI = 1;
    // Uri с указанным ID
    static final int URI_MODE_IDE_BY_WIFI_NAME = 2;
    // описание и создание UriMatcher
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(WiFiContract.CONTENT_AUTHORITY, WiFiContract.PATH_WIFI, URI_WIFI);
        uriMatcher.addURI(WiFiContract.CONTENT_AUTHORITY, WiFiContract.PATH_WIFI + "/#", URI_MODE_IDE_BY_WIFI_NAME);
    }

    final String LOG_TAG = "myLogs";
    DBHelper dbHelper;
    SQLiteDatabase db;

    public boolean onCreate() {
        Log.d(LOG_TAG, "onCreate");
        dbHelper = new DBHelper(getContext());
        return true;
    }

    // чтение
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "query, " + uri.toString());
        // проверяем Uri
        switch (uriMatcher.match(uri)) {
            case URI_WIFI: // общий Uri
                Log.d(LOG_TAG, "URI_WIFI");

                break;
            case URI_MODE_IDE_BY_WIFI_NAME: // Uri с ID
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_MODE_IDE_BY_WIFI_NAME, " + id);
                selection = WiFiContract.WiFiEntry.COLUME_WIFI_NAME + " = " + selection;
                break;

            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(WiFiContract.WiFiEntry.TABLE_NAME, projection, selection,
                selectionArgs, null, null, sortOrder);
        // просим ContentResolver уведомлять этот курсор
        // об изменениях данных в CONTACT_CONTENT_URI
        cursor.setNotificationUri(getContext().getContentResolver(),
                WiFiContract.BASE_CONTENT_URI);
        return cursor;
    }

    public Uri insert(Uri uri, ContentValues values) {
        Log.d(LOG_TAG, "insert, " + uri.toString());
        if (uriMatcher.match(uri) != URI_WIFI)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        db = dbHelper.getWritableDatabase();
        long rowID = db.insert(WiFiContract.WiFiEntry.TABLE_NAME, null, values);
        Uri resultUri = ContentUris.withAppendedId(WiFiContract.WiFiEntry.CONTENT_URI, rowID);
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(LOG_TAG, "delete, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_WIFI:
                Log.d(LOG_TAG, "URI_WIFI");
                break;
            case URI_MODE_IDE_BY_WIFI_NAME:
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_MODE_IDE_BY_WIFI_NAME, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = WiFiContract.WiFiEntry.COLUME_WIFI + " = " + id;
                } else {
                    selection = selection + " AND " + WiFiContract.WiFiEntry.COLUME_WIFI + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.delete(WiFiContract.WiFiEntry.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        Log.d(LOG_TAG, "update, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_WIFI:
                Log.d(LOG_TAG, "URI_WIFI");

                break;
            case URI_MODE_IDE_BY_WIFI_NAME:
                String id = uri.getLastPathSegment();
                Log.d(LOG_TAG, "URI_MODE_IDE_BY_WIFI_NAME, " + id);
                selection = WiFiContract.WiFiEntry.COLUME_WIFI_NAME + " = " + selection;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.update(WiFiContract.WiFiEntry.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    public String getType(Uri uri) {
        Log.d(LOG_TAG, "getType, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_WIFI:
                return WiFiContract.WiFiEntry.CONTENT_TYPE;
            case URI_MODE_IDE_BY_WIFI_NAME:
                return WiFiContract.WiFiEntry.CONTENT_ITEM_TYPE;
        }
        return null;
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(WiFiContract.WiFiEntry.DB_CREATE);

        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }


}
