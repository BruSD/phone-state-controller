package net.brusd.phonecontroller.AppDataBase;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


public class WiFiContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "net.brusd.phonecontroller";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.android.sunshine.app/weather/ is a valid path for
    // looking at weather data. content://com.example.android.sunshine.app/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.
    public static final String PATH_WIFI = "wifi";



    public static final class WiFiEntry implements BaseColumns {

        public static final Uri CONTENT_URI =BASE_CONTENT_URI.buildUpon().appendPath(PATH_WIFI).build();

        public static final String TABLE_NAME = "WiFi";

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_WIFI;

        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_WIFI;




        public static final String COLUME_WIFI = "_id";
        public static final String COLUME_WIFI_NAME = "_1name";
        public static final String COLUME_MODE_ID = "mode_id";

        static final String DB_CREATE = "create table " + TABLE_NAME + "("
                + COLUME_WIFI + " integer primary key autoincrement, "
                + COLUME_WIFI_NAME + " text, " + COLUME_MODE_ID + " text" + ");";


    }
}
