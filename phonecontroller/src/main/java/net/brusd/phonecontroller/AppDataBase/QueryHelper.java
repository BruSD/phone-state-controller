package net.brusd.phonecontroller.AppDataBase;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class QueryHelper {

    private final static String MODE_ID_BYWIFI_NAME =
            WiFiContract.WiFiEntry.TABLE_NAME +
                    "." + WiFiContract.WiFiEntry.COLUME_WIFI_NAME + " = ? ";


    private final static String WIFI_LIST_BY_MODE_ID =
            WiFiContract.WiFiEntry.TABLE_NAME +
                    "." + WiFiContract.WiFiEntry.COLUME_MODE_ID + " = ? ";

    public static int reletedModeToWiFi(String wifiName, int globalModeID, Context context) {
        int modeID = globalModeID;
        wifiName = wifiName.replace("\"", "");

        Cursor cursor = context.getContentResolver().query(WiFiContract.WiFiEntry.CONTENT_URI, null, MODE_ID_BYWIFI_NAME,
                new String[]{wifiName}, null);

        if (cursor != null && cursor.moveToFirst()) {
            modeID = cursor.getInt(cursor.getColumnIndex(WiFiContract.WiFiEntry.COLUME_MODE_ID));
        }
        return modeID;
    }

    public static void changeWiFiModeRelatione(String wifiName, int modeID, Context context) {
        wifiName = wifiName.replace("\"", "");

        Cursor cursor = context.getContentResolver().query(WiFiContract.WiFiEntry.CONTENT_URI, null, MODE_ID_BYWIFI_NAME,
                new String[]{wifiName}, null);

        ContentValues cv = new ContentValues();
        cv.put(WiFiContract.WiFiEntry.COLUME_WIFI_NAME, wifiName);
        cv.put(WiFiContract.WiFiEntry.COLUME_MODE_ID, modeID);

        if (cursor != null && cursor.moveToFirst()) {

            int cnt = context.getContentResolver().update(WiFiContract.WiFiEntry.CONTENT_URI, cv, MODE_ID_BYWIFI_NAME, new String[]{wifiName});
            Log.d("BruSD", "update, count = " + cnt);
        } else {
            Uri newUri = context.getContentResolver().insert(WiFiContract.WiFiEntry.CONTENT_URI, cv);
            Log.d("BruSD", "insert, result Uri : " + newUri.toString());
        }
    }

    public static Cursor getCursoreWithWifiReletedToMode(int modeID, Context context) {


        Cursor cursor = context.getContentResolver().query(WiFiContract.WiFiEntry.CONTENT_URI, null, WIFI_LIST_BY_MODE_ID,
                new String[]{modeID+""}, null);


        return cursor;
    }

    public static int getCountOfWifiReletedToMode(int modeSilent, Activity parentActivity) {
        int count = 0;


        return count;
    }

    public static void deleteWiFi(String wifiName, Context context) {

        int cnt = context.getContentResolver().delete(WiFiContract.WiFiEntry.CONTENT_URI, MODE_ID_BYWIFI_NAME,
                new String[]{wifiName});
    }
}
