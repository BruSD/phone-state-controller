package net.brusd.phonecontroller.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class RandomJokeLoader extends AsyncTaskLoader<String> {

    String url = "https://webknox-jokes.p.mashape.com/jokes/random";

    public RandomJokeLoader(Context context) {
        super(context);
    }

    @Override
    public String loadInBackground() {
        return getJoke();
    }

    private String getJoke() {
        OkHttpClient client = new OkHttpClient();
        String joke = "";

        Request request = new Request.Builder().header("X-Mashape-Key", "U9KRoJxtS9mshR7noWSBI7gmteOVp1gqNtZjsnnuvW3OrrzLos")
                .url(url)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();


            String str = response.body().string();

            JSONObject json = new JSONObject(str);

            joke = json.getString("joke");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return joke;
    }
}
