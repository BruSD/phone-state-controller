package net.brusd.phonecontroller.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.AppDataBase.WiFiProvider;
import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.utils.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class WiFiListLoader extends AsyncTaskLoader<ArrayList<HashMap<String, String>>> {

    private static WiFiProvider wiFiProvider = null;
    private Context context;

    public WiFiListLoader(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public ArrayList<HashMap<String, String>> loadInBackground() {
        return loadWifiList();
    }

    private ArrayList<HashMap<String, String>> loadWifiList() {
        ArrayList<HashMap<String, String>> hashMaps = new ArrayList<>();

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {

            wiFiProvider = new WiFiProvider();
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration config : configs) {

                int modeID = QueryHelper.reletedModeToWiFi(config.SSID, SharedPreferences.getGlobalVolumeMode(context), context);
                HashMap<String, String> temp = new HashMap<>();
                String wifiName = config.SSID.replace("\"", "");
                temp.put(Constant.WIFI_NAME, wifiName);
                temp.put(Constant.MODE_NAME, String.valueOf(modeID));

                hashMaps.add(temp);
            }

        }

        return hashMaps;

    }
}
