package net.brusd.phonecontroller.dialog;

import android.app.Dialog;
import android.content.Context;

import android.view.View;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;

import android.widget.RadioButton;
import android.widget.RadioGroup;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.AppDataBase.WiFiProvider;
import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.R;
import net.brusd.phonecontroller.utils.SharedPreferences;
import net.brusd.phonecontroller.utils.VolumeLevelController;

import java.util.HashMap;


public class ChosenDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Context context;
    private RadioGroup radiogroup;
    private static WiFiProvider wiFiProvider = null;
    private String wifi;
    private Button yes, no;
    private int modeID;
    private HashMap<String, String> wifiHashMap;

    private RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {

                case R.id.radio_full_mode:
                    modeID = Constant.MODE_FULL;
                    break;
                case R.id.radio_medium_mode:
                    modeID = Constant.MODE_MEDIUM;
                    break;
                case R.id.radio_silent_mode:
                    modeID = Constant.MODE_SILENT;
                    break;
            }
        }
    };


    public ChosenDialog(Context context, String wifiName) {
        super(context);
        this.context = context;
        this.wifi = wifiName;
        wiFiProvider = new WiFiProvider();

    }

    public ChosenDialog(Context context, HashMap<String, String> wifiName) {
        super(context);
        this.context = context;
        wifiHashMap = wifiName;
        this.wifi = wifiName.get(Constant.WIFI_NAME);
        wiFiProvider = new WiFiProvider();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choisen);

        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        radiogroup = (RadioGroup) findViewById(R.id.radio_group);

        radiogroup.setOnCheckedChangeListener(onCheckedChangeListener);
        initChosenRB();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    private void initChosenRB() {
        modeID = QueryHelper.reletedModeToWiFi(wifi, SharedPreferences.getGlobalVolumeMode(context), context);
        switch (modeID) {
            case Constant.MODE_FULL:
                RadioButton radioFullButton = (RadioButton) findViewById(R.id.radio_full_mode);
                radioFullButton.setChecked(true);
                break;
            case Constant.MODE_MEDIUM:
                RadioButton radioMediumButton = (RadioButton) findViewById(R.id.radio_medium_mode);
                radioMediumButton.setChecked(true);
                break;
            case Constant.MODE_SILENT:
                RadioButton radioSilentButton = (RadioButton) findViewById(R.id.radio_silent_mode);
                radioSilentButton.setChecked(true);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                QueryHelper.changeWiFiModeRelatione(wifi, modeID, context);
                if (wifiHashMap != null){
                    wifiHashMap.put(Constant.MODE_NAME, String.valueOf(QueryHelper.reletedModeToWiFi(wifi, SharedPreferences.getGlobalVolumeMode(context), context)));
                }else {
                    VolumeLevelController.changVolumeLevel(context, modeID);
                }
                dismiss();
                break;
            case R.id.btn_no:
                dismiss();
                break;
        }
    }
}
