package net.brusd.phonecontroller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;

import net.brusd.phonecontroller.fragments.ModeSettingFragment;
import net.brusd.phonecontroller.fragments.ModesFragment;


public class ModeSettingActivity extends Activity {
    public ContentType currentFragment = ContentType.MODES;
    private FragmentManager fragmentManager;
    private FragmentChangeBroadcastReceiver fragmentChangeBroadcastReceiver;
    private boolean mTwoPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_setting);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentManager = getFragmentManager();

        if (findViewById(R.id.fragment_modes) != null) {
            mTwoPanel = true;
            currentFragment = ContentType.MODE_SETTING;
            int modeID = 0;
            Bundle bundle = new Bundle();
            bundle.putInt(Constant.MODE_ID, modeID);
            setFragment(ContentType.MODE_SETTING, bundle);
        } else {
            mTwoPanel = false;
        }

        if (savedInstanceState == null) {
            setFragment(currentFragment, null);
        }

        fragmentChangeBroadcastReceiver = new FragmentChangeBroadcastReceiver();

    }

    @Override
    protected void onResume() {
        super.onResume();


        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.SET_MODE_SETTING);
        registerReceiver(fragmentChangeBroadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        if (fragmentChangeBroadcastReceiver != null)
            unregisterReceiver(fragmentChangeBroadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFragment(ContentType contentType, Bundle bundle) {
        final Fragment fragmentOld = getFragmentManager().findFragmentById(R.id.container);

        final Fragment fragment = fragmentFromContentType(contentType);


        if (!fragment.equals(fragmentOld)) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragment.setArguments(bundle);
            ft.replace(R.id.container, fragment);
            if (bundle != null && !mTwoPanel) {
                ft.addToBackStack(fragment.getClass().getName());
            } else {
                fragmentManager.popBackStack();
            }
            ft.commitAllowingStateLoss();
        }

    }

    private Fragment fragmentFromContentType(ContentType contentType) {
        Fragment fragment;
        switch (contentType) {

            case MODES: {
                fragment = new ModesFragment();
                currentFragment = ContentType.MODES;
                break;
            }

            case MODE_SETTING: {
                fragment = new ModeSettingFragment();
                currentFragment = ContentType.MODE_SETTING;
                break;
            }


            default: {
                return null;
            }

        }
        return fragment;
    }

    public enum ContentType {
        MODES, MODE_SETTING
    }

    class FragmentChangeBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constant.SET_MODE_SETTING)) {
                int modeID = intent.getIntExtra(Constant.MODE_ID, 0);
                Bundle bundle = new Bundle();
                bundle.putInt(Constant.MODE_ID, modeID);
                setFragment(ContentType.MODE_SETTING, bundle);

            }
        }
    }
}
