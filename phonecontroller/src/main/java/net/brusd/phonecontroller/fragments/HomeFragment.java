package net.brusd.phonecontroller.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.AppDataBase.WiFiProvider;
import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.R;
import net.brusd.phonecontroller.dialog.ChosenDialog;
import net.brusd.phonecontroller.loaders.RandomJokeLoader;
import net.brusd.phonecontroller.utils.SharedPreferences;
import net.brusd.phonecontroller.utils.VolumeLevelController;


public class HomeFragment extends Fragment implements LoaderManager.LoaderCallbacks {
    private static final String APPLICAITON_ID = "542d3fc223a7211400000000";
    private static final int LOADER_JOKE = 13;
    private static WiFiProvider wiFiProvider = null;
    private View rootView;
    private Activity parentActivity;
    private String ssid = null;
    private TextView wifiNameTextView;
    private ImageView modeTypeImageView, editModeAssocieteImageView;
    private ChosenDialog chosenDialog;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showChosenDialog();

        }
    };

    private void showChosenDialog() {
        chosenDialog = new ChosenDialog(parentActivity, ssid);
        chosenDialog.setCancelable(false);
        chosenDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                initModeTypeImageView(QueryHelper.reletedModeToWiFi(ssid, SharedPreferences.getGlobalVolumeMode(parentActivity), parentActivity));
            }
        });
        chosenDialog.show();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        parentActivity = getActivity();
        wifiNameTextView = (TextView) rootView.findViewById(R.id.current_connected_wifi_text_view);
        modeTypeImageView = (ImageView) rootView.findViewById(R.id.mode_type_image_view);
        editModeAssocieteImageView = (ImageView) rootView.findViewById(R.id.edit_mode_image_view);

        getLoaderManager().initLoader(LOADER_JOKE, null, HomeFragment.this);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initNetworkData();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    private void initNetworkData() {

        ConnectivityManager connManager = (ConnectivityManager) parentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) parentActivity.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !(connectionInfo.getSSID().equals(""))) {
                wiFiProvider = new WiFiProvider();
                ssid = connectionInfo.getSSID();
                ssid = ssid.replace("\"", "");
                initModeTypeImageView(QueryHelper.reletedModeToWiFi(ssid, SharedPreferences.getGlobalVolumeMode(parentActivity), parentActivity));
                editModeAssocieteImageView.setOnClickListener(onClickListener);
            }
        } else {
            ssid = parentActivity.getString(R.string.no_wifi_connection_string);
        }
        wifiNameTextView.setText(ssid);
    }

    private void initModeTypeImageView(int modeID) {
        VolumeLevelController.changVolumeLevel(parentActivity, modeID);
        switch (modeID) {
            case Constant.MODE_FULL:
                modeTypeImageView.setBackgroundColor(getResources().getColor(R.color.full_mode));
                modeTypeImageView.setImageDrawable(parentActivity.getResources().getDrawable(android.R.drawable.ic_lock_silent_mode_off));
                break;
            case Constant.MODE_MEDIUM:
                modeTypeImageView.setBackgroundColor(getResources().getColor(R.color.medium_mode));
                modeTypeImageView.setImageDrawable(parentActivity.getResources().getDrawable(android.R.drawable.ic_lock_silent_mode_off));
                break;
            case Constant.MODE_SILENT:
                modeTypeImageView.setBackgroundColor(getResources().getColor(R.color.silent_mode));
                modeTypeImageView.setImageDrawable(parentActivity.getResources().getDrawable(android.R.drawable.ic_lock_silent_mode));
                break;

        }


    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = new Loader(parentActivity);
        if (((ConnectivityManager) parentActivity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            if (LOADER_JOKE == id) {
                loader = new RandomJokeLoader(parentActivity);
                loader.forceLoad();
            }
        }
        return loader;

    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
    TextView jokeTextView = (TextView)rootView.findViewById(R.id.joke_of_the_day_text_view);
        String joke = data.toString();
        jokeTextView.setText(joke);

    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
