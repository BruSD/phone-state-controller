package net.brusd.phonecontroller.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.brusd.phonecontroller.R;


public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.global_preferences);
    }

}
