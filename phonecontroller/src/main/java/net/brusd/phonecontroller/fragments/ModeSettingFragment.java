package net.brusd.phonecontroller.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.Constant;
import net.brusd.phonecontroller.R;
import net.brusd.phonecontroller.utils.ModeRelatedCursoreAdapter;
import net.brusd.phonecontroller.utils.SharedPreferences;

public class ModeSettingFragment extends Fragment {

    private View rootView;
    private Activity parrentActivity;
    private TextView ringValueTextView, alarmValueTextView, notificationValueTextView, modeNameTextView;

    private ListView listView;
    private String modeName;

    private int modeID = 0;
    private int ringValue, alarmValue, notificationValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings_mode, container, false);
        parrentActivity = getActivity();
        Bundle arguments = getArguments();
        if (arguments != null) {
            modeID = arguments.getInt(Constant.MODE_ID);
        }


        initDataToView();
        initView();
        setAllData();
        return rootView;
    }


    private void initView() {
        modeNameTextView = (TextView) rootView.findViewById(R.id.mode_name_setting_mode_frag_text_veiw);
        //Values
        ringValueTextView = (TextView) rootView.findViewById(R.id.ring_value_text_view);
        alarmValueTextView = (TextView) rootView.findViewById(R.id.alarm_value_text_view);
        notificationValueTextView = (TextView) rootView.findViewById(R.id.notification_value_text_view);
        //switch
        listView = (ListView) rootView.findViewById(R.id.wifi_in_this_mode_list_view);

        ModeRelatedCursoreAdapter simpleCursorAdapter = new ModeRelatedCursoreAdapter(parrentActivity,
                QueryHelper.getCursoreWithWifiReletedToMode(modeID, parrentActivity),
                0
        );

        listView.setAdapter(simpleCursorAdapter);
    }

    private void initDataToView() {
        switch (modeID) {
            case Constant.MODE_FULL:
                initFullModeData();
                break;
            case Constant.MODE_MEDIUM:
                initMediumMode();
                break;
            case Constant.MODE_SILENT:
                initSilentMode();
                break;
        }
    }


    //region INIT_DATA
    private void initFullModeData() {
        modeName = parrentActivity.getString(R.string.full_volume_mode_string);

        ringValue = SharedPreferences.getFullRingVolume(parrentActivity);
        alarmValue = SharedPreferences.getFullAlarmVolume(parrentActivity);
        notificationValue = SharedPreferences.getFullNotification(parrentActivity);

    }

    private void initMediumMode() {
        modeName = parrentActivity.getString(R.string.medium_volume_mode_string);

        ringValue = SharedPreferences.getMediumRingVolume(parrentActivity);
        alarmValue = SharedPreferences.getMediumAlarmVolume(parrentActivity);
        notificationValue = SharedPreferences.getMediumNotification(parrentActivity);

    }

    private void initSilentMode() {
        modeName = parrentActivity.getString(R.string.silent_volume_mode_string);

        ringValue = SharedPreferences.getSilentRingVolume(parrentActivity);
        alarmValue = SharedPreferences.getSilentAlarmVolume(parrentActivity);
        notificationValue = SharedPreferences.getSilentNotification(parrentActivity);

    }

    private void setAllData() {
        modeNameTextView.setText(modeName);

        ringValueTextView.setText(ringValue + "%");
        alarmValueTextView.setText(alarmValue + "%");
        notificationValueTextView.setText(notificationValue + "%");

    }
    //endregion


}
