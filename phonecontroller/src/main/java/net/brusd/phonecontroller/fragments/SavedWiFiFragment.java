package net.brusd.phonecontroller.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import net.brusd.phonecontroller.AppDataBase.WiFiProvider;
import net.brusd.phonecontroller.R;
import net.brusd.phonecontroller.loaders.WiFiListLoader;
import net.brusd.phonecontroller.utils.NetworkSimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;


public class SavedWiFiFragment extends Fragment implements LoaderManager.LoaderCallbacks {
    private String TAG = "PhoneStateController";
    private View rootView;
    private Activity parentActivity;

    private ArrayList<HashMap<String, String>> networkList = new ArrayList<>();
    private NetworkSimpleAdapter networkSimpleAdapter;
    private ListView listView;

    private final static int LOADER_WIFI_NETWORK_GETER = 0;
    private static WiFiProvider wiFiProvider = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentActivity = getActivity();
        rootView = inflater.inflate(R.layout.fragment_saved_wifi, container, false);
        listView = (ListView) rootView.findViewById(R.id.network_list);



        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getLoaderManager().restartLoader(LOADER_WIFI_NETWORK_GETER, null, this);
    }



    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = new Loader(parentActivity);

        switch (id) {

            case LOADER_WIFI_NETWORK_GETER:
                ConnectivityManager connManager = (ConnectivityManager) parentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                TextView notificationTextView = (TextView) rootView.findViewById(R.id.no_connection_notification_text_view);
                if (networkInfo.isConnected()) {
                    notificationTextView.setVisibility(View.GONE);
                    loader = new WiFiListLoader(parentActivity);
                    loader.forceLoad();

                } else {
                    notificationTextView.setVisibility(View.VISIBLE);
                    notificationTextView.setText(parentActivity.getString(R.string.no_wifi_connection_string));
                }
                break;
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        int loaderID = loader.getId();
        switch (loaderID) {
            case LOADER_WIFI_NETWORK_GETER:
                networkList = (ArrayList<HashMap<String, String>>) data;
                networkSimpleAdapter = new NetworkSimpleAdapter(parentActivity, networkList, 0, null, null);
                listView.setAdapter(networkSimpleAdapter);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
