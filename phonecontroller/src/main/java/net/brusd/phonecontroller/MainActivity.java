package net.brusd.phonecontroller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.brusd.phonecontroller.fragments.AboutFragment;
import net.brusd.phonecontroller.fragments.HomeFragment;
import net.brusd.phonecontroller.fragments.SavedWiFiFragment;
import net.brusd.phonecontroller.fragments.SettingsFragment;

import java.util.ArrayList;


public class MainActivity extends Activity {

    public ContentType currentFragment = ContentType.HOME;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private boolean mTwoPanel;
    private int selectedPosition = 0;
    private static final String POSITION_KEY = "POSITION_KEY";
    private ListView listView;
    private FragmentManager fragmentManager;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(POSITION_KEY, selectedPosition);

        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(POSITION_KEY)) {
            selectedPosition = savedInstanceState.getInt(POSITION_KEY);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerTitle = this.getString(R.string.drawer_open);
        mTitle = mDrawerTitle;
        if (findViewById(R.id.drawer_layout) != null) {
            initialDrawerContent();
            mTwoPanel = false;
        } else {
            initButton();
            mTwoPanel = true;
        }

        fragmentManager = getFragmentManager();
        if (savedInstanceState == null) {
            setFragment(currentFragment, null);
        }

    }

    private void initialDrawerContent() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.app_name  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                initDrawerButton(drawerView);
                getActionBar().setTitle(mDrawerTitle);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (!mTwoPanel) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!mTwoPanel) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    public void initButton() {
        String[] groups = getResources().getStringArray(R.array.menu_item);

        ArrayAdapter menuAdapter = new ArrayAdapter<String>(this, R.layout.list_item_menu, R.id.text1, groups);


        // Get a reference to the ListView, and attach this adapter to it.
        listView = (ListView) findViewById(R.id.list_menu_item);
        listView.setAdapter(menuAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                onClickMenu(position);
            }
        });

        listView.setItemChecked(selectedPosition, true);

    }



    public void initDrawerButton(View drawerView) {
        String[] groups = getResources().getStringArray(R.array.menu_item);

        ArrayAdapter<String> menuAdapter = new ArrayAdapter<String>(this, R.layout.list_item_menu,R.id.text1, groups);


        // Get a reference to the ListView, and attach this adapter to it.
        listView = (ListView) drawerView.findViewById(R.id.list_menu_item);
        listView.setAdapter(menuAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                onClickMenu(position);
            }
        });

        listView.setItemChecked(selectedPosition, true);
    }


    public void onClickMenu(int position) {
        if (!mTwoPanel) {
            mDrawerLayout.closeDrawers();
        }

        switch (position) {
            case 0:
                setFragment(ContentType.HOME, null);
                selectedPosition = position;
                break;
            case 1:
                setFragment(ContentType.SAVED_WIFI, null);
                selectedPosition = position;
                break;
            case 2:
                startActivity(new Intent(MainActivity.this, ModeSettingActivity.class));
                break;
            case 3:
                setFragment(ContentType.SETTING, null);
                selectedPosition = position;
                break;
            case 4:
                break;

        }
    }

    private void setFragment(ContentType contentType, Bundle bundle) {
        final Fragment fragmentOld = getFragmentManager().findFragmentById(R.id.fragment_container);

        final Fragment fragment = fragmentFromContentType(contentType);


        if (!fragment.equals(fragmentOld)) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragment_container, fragment);
            if (bundle != null) {
                ft.addToBackStack(fragment.getClass().getName());
            } else {
                fragmentManager.popBackStack();
            }
            ft.commitAllowingStateLoss();
        }

    }

    private Fragment fragmentFromContentType(ContentType contentType) {
        Fragment fragment;
        switch (contentType) {
            case HOME: {
                fragment = new HomeFragment();

                currentFragment = ContentType.HOME;
                break;

            }

            case SAVED_WIFI: {
                fragment = new SavedWiFiFragment();
                currentFragment = ContentType.SAVED_WIFI;
                break;
            }

            case SETTING: {
                fragment = new SettingsFragment();
                currentFragment = ContentType.SETTING;
                break;
            }

            case ABOUT: {
                fragment = new AboutFragment();
                currentFragment = ContentType.ABOUT;
                break;
            }
            default: {
                return null;
            }

        }
        return fragment;
    }


    public enum ContentType {
        HOME, SAVED_WIFI, SETTING, ABOUT
    }


}


