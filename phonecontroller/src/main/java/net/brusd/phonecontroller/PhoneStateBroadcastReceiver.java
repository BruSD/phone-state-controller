package net.brusd.phonecontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

import net.brusd.phonecontroller.AppDataBase.QueryHelper;
import net.brusd.phonecontroller.utils.NotificationGenerator;
import net.brusd.phonecontroller.utils.VolumeLevelController;


public class PhoneStateBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo networkInfo = intent
                    .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (networkInfo.isConnected()) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                String wifiName  = connectionInfo.getSSID();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                int globalModeID = Integer.valueOf(prefs.getString(context.getString(R.string.global_mode_key), "0"));
                int modeID = QueryHelper.reletedModeToWiFi(wifiName, globalModeID, context);

                VolumeLevelController.changVolumeLevel(context, modeID );

                if(prefs.getBoolean(context.getString(R.string.notification), true)) {
                    NotificationGenerator.createNotification(context, modeID);
                }
            } else {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                int disconectedMode =Integer.valueOf(prefs.getString(context.getString(R.string.disconected_volume_mode_key), "0"));
                VolumeLevelController.changVolumeLevel(context, disconectedMode );

                if(prefs.getBoolean(context.getString(R.string.notification), true)) {
                    NotificationGenerator.createNotification(context, disconectedMode);
                }
            }


        }

    }


}
